package com.androidexample.creatlist;

public class CreateModel  {
    private String mlistmenu;
    private int mimageid;


     CreateModel(String listmenu, int imageid){
        mlistmenu=listmenu;
        mimageid=imageid;
    }

    public String getlistmenu(){
        return mlistmenu;

    }

    public String getMlistmenu() {
        return mlistmenu;
    }

    public void setMlistmenu(String mlistmenu) {
        this.mlistmenu = mlistmenu;
    }

    public int getMimageid() {
        return mimageid;
    }

    public void setMimageid(int mimageid) {
        this.mimageid = mimageid;
    }

    public int getimageid(){
        return mimageid;

    }

}