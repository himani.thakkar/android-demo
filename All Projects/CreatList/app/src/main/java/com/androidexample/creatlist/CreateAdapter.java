package com.androidexample.creatlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CreateAdapter extends ArrayAdapter<CreateModel> {
    private static final String LOG_TAG=CreateAdapter.class.getSimpleName();
    public CreateAdapter(Activity context, ArrayList<CreateModel> al){
        super(context,0,al);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView=convertView;

        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_menu,parent,false);
        }
        CreateModel currentcreate=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_text);
        tv.setText(currentcreate.getlistmenu());


    ImageView iv=(ImageView)listItemView.findViewById(R.id.iv_img);
        iv.setImageResource(currentcreate.getimageid());

        return listItemView;


    }
}


