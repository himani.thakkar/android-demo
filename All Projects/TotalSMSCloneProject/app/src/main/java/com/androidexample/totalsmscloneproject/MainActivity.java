package com.androidexample.totalsmscloneproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidexample.totalsmscloneproject.fragments.HomeFragment;

public abstract class MainActivity extends AppCompatActivity implements Maininterface {
    FrameLayout container;
    ImageView img_hometab;
    private Toolbar toolbar;

    private TextView tab_label;
    private int[] tabIcons = {
            R.drawable.homegrey,
            R.drawable.callgrey,
            R.drawable.smsgrey,
            R.drawable.configurationgrey,
            R.drawable.moregrey
    };

    private int[] navLabels = {
            R.string.home,
            R.string.call,
            R.string.SMS,
            R.string.configuration,
            R.string.menu
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
        @Override

        public void Openhome () {
            container.setVisibility(View.VISIBLE);
            replaceFragment(new HomeFragment(),R.id.container,HomeFragment.class.getSimpleName(),false);
            }

            public void Opencall(){
           container.setVisibility(View.VISIBLE);
            }

    public void replaceFragment(Fragment mFragment, int id, String tag, boolean addToStack) {
        FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
        mTransaction.replace(id, mFragment);
//        hideKeyboard();
        if (addToStack) {
            mTransaction.addToBackStack(tag);
        }
        mTransaction.commitAllowingStateLoss();
    }

 public void setColorunselrcted(ImageView img_hometab){
     this.img_hometab = img_hometab;

 }
    public ImageView getColorunselrcted(){
        return img_hometab;
    }
 }


