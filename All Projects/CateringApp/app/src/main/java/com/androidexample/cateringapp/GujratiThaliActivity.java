package com.androidexample.cateringapp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class GujratiThaliActivity extends AppCompatActivity {
    android.support.v7.widget.Toolbar tl;
    LinearLayout layout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.GujratiMenu);
        setContentView(R.layout.activity_gujrati_thali);

        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        layout = (LinearLayout) findViewById(R.id.hello);





                   final ArrayList<WordModel> words=new ArrayList<WordModel>();
                 words.add(new WordModel("Bhindi","Fry Bhindi,Tamatar,Green Mirchi",R.drawable.bh));
        words.add(new WordModel("Methi Papad","Methi,Papad,Green Mirchi",R.drawable.mp));
        words.add(new WordModel("Oondhiya","Tamatar,Bateka,Kanda",R.drawable.o1));
        words.add(new WordModel("Sev Tameta","Sev,Tamatar,Green Mirchi",R.drawable.st));
        words.add(new WordModel("Raitu","Dahi,Green-Mirchi",R.drawable.raitu));
            WordAdapter adapter=new WordAdapter(this,words);
                  ListView listView = (ListView) findViewById(R.id.lv_guj);
                  listView.setAdapter(adapter);
                  Log.i("myInfoTag","infomsg");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            WordModel wm = words.get(position);
            Intent intent = new Intent(GujratiThaliActivity.this, GujPrice.class);

            intent.putExtra("message",wm.getlistmenu());
            intent.putExtra( "resId",wm.getimgid());
            startActivity(intent);
        }
    });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}