package com.androidexample.cateringapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class GujratiThali extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gujrati_thali);


                   ArrayList<WordJava> words=new ArrayList<WordJava>();
                 words.add(new WordJava("Bhindi","fry bhindi,tamatar,green mirchi",R.drawable.g1));
        words.add(new WordJava("Methi Papad","methi,papad,green mirchi",R.drawable.g1));
        words.add(new WordJava("Oondhiya","tamatar,bateka,kanda",R.drawable.g1));
        words.add(new WordJava("Sev Tameta","sev,tamatar,green mirchi",R.drawable.g1));
        words.add(new WordJava("Raitu","Dahi,green-mirchi",R.drawable.g1));



               WordAdapter adapter=new WordAdapter(this,words);
                  ListView listView = (ListView) findViewById(R.id.lv_guj);
                  listView.setAdapter(adapter);
                  Log.i("myInfoTag","infomsg");

    }

}