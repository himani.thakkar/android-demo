package com.androidexample.cateringapp;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class WordAdapter extends ArrayAdapter<WordModel> {
    private static final String LOG_TAG=WordAdapter.class.getSimpleName();
    public WordAdapter(Activity context, ArrayList<WordModel>words){
        super(context,0,words);
        }
        @Override
         public View getView(int position, View convertView, ViewGroup parent) {
         View listItemView=convertView;

        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }
        WordModel currentwordjava=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_menu);
        tv.setText(currentwordjava.getlistmenu());

        TextView tv1=(TextView) listItemView.findViewById(R.id.tv_submenu);
        tv1.setText(currentwordjava.getlistsubmenu());



        ImageView iconView = (ImageView) listItemView.findViewById(R.id.img_guj);

        if (currentwordjava.hasImage()) {
            iconView.setImageResource(currentwordjava.getimgid());
            iconView.setVisibility(View.VISIBLE);
        } else {
            iconView.setVisibility(View.GONE);
        }
        return listItemView;


    }
}
