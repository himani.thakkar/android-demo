package com.androidexample.cateringapp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_gujarati_thali;
    TextView tv_panjabi_thali;
    TextView tv_chainese;
    TextView tv_sauth_indian;
    android.support.v7.widget.Toolbar tl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(getResources().getString(R.string.FoodOrder));

        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.tb);
        setSupportActionBar(tl);
        initViews();
        clickListeners();
    }

    private void clickListeners() {
        tv_gujarati_thali.setOnClickListener(this);
         tv_panjabi_thali.setOnClickListener(this);
         tv_sauth_indian.setOnClickListener(this);
         tv_chainese.setOnClickListener(this);

    }

    private void initViews() {
        tv_gujarati_thali = findViewById(R.id.tv_gujarati_thali);
        tv_panjabi_thali=findViewById(R.id.tv_panjabi_thali);
        tv_chainese=findViewById(R.id.tv_chainese);
        tv_sauth_indian=findViewById(R.id.tv_sauth_indian);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_gujarati_thali:
                Intent i=new Intent(this,GujratiThaliActivity.class);
                startActivity(i);
                break;

            case R.id.tv_panjabi_thali:
                Intent i1=new Intent(this,PanjabiThaliActivity.class);
                startActivity(i1);
                break;

            case R.id.tv_chainese:
            Intent i2=new Intent(this,ChaineseActivity.class);
            startActivity(i2);
            break;

            case R.id.tv_sauth_indian:
                Intent i3=new Intent(this,SauthIndianActivity.class);
                startActivity(i3);
                break;


        }

    }


}