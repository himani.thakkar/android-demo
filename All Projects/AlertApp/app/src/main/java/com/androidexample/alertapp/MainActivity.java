package com.androidexample.alertapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn;

    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        OnClickListener();
    }


        public void initView(){
            btn=findViewById(R.id.btn);


        }

        public void OnClickListener(){
         btn.setOnClickListener(this);

        }


         @Override
    public void onClick(View v) {
             AlertDialog.Builder a_Builder = new AlertDialog.Builder(MainActivity.this);
             a_Builder.setMessage("Want to close this app").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int which) {
                     finish();
                 }
             })
                     .setNegativeButton("No", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             dialog.cancel();
                         }
                     });
             AlertDialog alert = a_Builder.create();
             alert.setTitle("Alert");
             alert.show();


         }



    }


