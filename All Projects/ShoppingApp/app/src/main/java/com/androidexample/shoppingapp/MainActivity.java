package com.androidexample.shoppingapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_clth;
    TextView tv_ftw;
    Button btn_click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        ClothesCatagoryAdapter adapter = new ClothesCatagoryAdapter(this,getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        initViews();
        clickListeners();
    }

    private void  clickListeners()  {

        tv_clth.setOnClickListener(this);
        tv_ftw.setOnClickListener(this);
     //   btn_click.setOnClickListener(this);
    }

    private void initViews() {
        tv_clth = findViewById(R.id.tv_clth);
        tv_ftw = findViewById(R.id.tv_ftw);
      //  btn_click.findViewById(R.id.btn_click);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_clth:
                Intent i=new Intent(this,ClothesActivity.class);
                startActivity(i);
                break;

            case R.id.tv_ftw:
                Intent i1=new Intent(this,Footware.class);
                startActivity(i1);
                break;

          /*  case R.id.btn_click:
                Intent i2=new Intent(this,WomanFragment.class);
                startActivity(i2);
                break; */


        }
    }
}

