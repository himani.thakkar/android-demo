package com.androidexample.shoppingapp;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ClothesCatagoryAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public ClothesCatagoryAdapter(Context context,FragmentManager fm) {
        super(fm);
        mContext = context;

    }


    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.ClothesCatagory_Tops);
        } else if (position == 1) {
            return mContext.getString(R.string.ClothesCatagory_Jeans);
        }  else {
            return mContext.getString(R.string.ClothesCatagory_Skirt);
        }

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new WomanFragment();
        } else if (position == 1){
            return new ManFragment();
        } else {
            return new ChildrensFragment();
        }
    }
    @Override
    public int getCount() {
        return 3;
    }

    }


