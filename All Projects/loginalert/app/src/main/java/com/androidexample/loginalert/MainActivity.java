package com.androidexample.loginalert;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn_ln;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mbutton = (Button) findViewById(R.id.btn_ln);
        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder a_Builder = new AlertDialog.Builder(MainActivity.this);
                View mview = getLayoutInflater().inflate(R.layout.login, null);
                final EditText edt = (EditText) mview.findViewById(R.id.edt_username);
                final EditText edtp = (EditText) mview.findViewById(R.id.edt_pass);
                Button btn = (Button) mview.findViewById(R.id.btn_login);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!edt.getText().toString().isEmpty() && !edtp.getText().toString().isEmpty()) {
                            Toast.makeText(MainActivity.this, "Login Successfull", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Empty Any field", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                a_Builder.setView(mview);
                AlertDialog alertDialog = a_Builder.create();
                alertDialog.show();

            }

        });


    }
}


