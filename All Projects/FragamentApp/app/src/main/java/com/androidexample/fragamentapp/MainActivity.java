package com.androidexample.fragamentapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_woman;
    Button btn_man;
    Button btn_child;
    Button btn_view;
    LinearLayout layout;
    android.support.v7.widget.Toolbar tl;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tl = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }




        layout = (LinearLayout) findViewById(R.id.ll_layout);


//        setSupportActionBar(tl);
//   }


//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        Configuration configInfo = getResources().getConfiguration();
//        if (configInfo.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            WomanFragment wf = new WomanFragment();
//            fragmentTransaction.replace(R.id.ll_content, wf).addToBackStack("tag");
//        } else
//
//        {
//            ManFragment mf = new ManFragment();
//            fragmentTransaction.replace(R.id.ll_content, mf);
//
//        }
//        fragmentTransaction.commit();


        initViews();
        clickListeners();
    }

    private void clickListeners() {
        btn_woman.setOnClickListener(this);
        btn_man.setOnClickListener(this);
        btn_child.setOnClickListener(this);
        btn_view.setOnClickListener(this);
    }

    private void initViews() {
        btn_woman = findViewById(R.id.btn_woman);
        btn_man = findViewById(R.id.btn_man);
        btn_child = findViewById(R.id.btn_child);
        btn_view = findViewById(R.id.btn_view);


    }


    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.btn_woman) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ll_content, new WomanFragment());
            layout.setVisibility(View.GONE);

            fragmentTransaction.commit();
        } else if (v.getId() == R.id.btn_man) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ll_content, new WomanFragment());
            layout.setVisibility(View.GONE);

            fragmentTransaction.commit();

        } else if (v.getId() == R.id.btn_child) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ll_content, new WomanFragment());
            fragmentTransaction.commit();
        } else if (v.getId() == R.id.btn_view) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ll_content, new WomanFragment());
            fragmentTransaction.commit();
        }



    }





    @Override
    public boolean onOptionsItemSelected(MenuItem menuitem) {
        // handle arrow click here
        if (menuitem.getItemId() == android.R.id.home)
        {
            layout.setVisibility(View.VISIBLE);
            Toast toast = Toast.makeText(getApplicationContext(),
                    "back to home",
                    Toast.LENGTH_SHORT);

            toast.show();

        }

            return super.onOptionsItemSelected(menuitem);
    }


}