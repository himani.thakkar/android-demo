package com.androidexample.fragamentapp;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ShopAdapter extends FragmentPagerAdapter {


    private Context mContext;

    public ShopAdapter(Context context,FragmentManager fm) {
        super(fm);
        mContext = context;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new WomanFragment();
        } else if (position == 1){
            return new ManFragment();
        } else if(position==2) {
            return new ChildFragment();
        }
        else {
            return new ViewallFragment();

        }
    }
    @Override
    public int getCount() {
        return 4;
    }

}



