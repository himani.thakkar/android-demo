package com.androidexample.totalsmsdemo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TotalSmsAdapter extends ArrayAdapter<TotalSmsModel> {

    Context context;
    ArrayList<TotalSmsModel> data;

    private static final String LOG_TAG=TotalSmsAdapter.class.getSimpleName();
    public TotalSmsAdapter(Activity context, ArrayList<TotalSmsModel> data){
        super(context,0,data);
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView=convertView;

        if(listItemView==null){
            listItemView=LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }
        TotalSmsModel currentTotalSmsjava=getItem(position);
        TextView tv=(TextView) listItemView.findViewById(R.id.tv_menu);
        tv.setText( currentTotalSmsjava.getcountryname());

        TextView tv1=(TextView) listItemView.findViewById(R.id.tv_submenu);
        tv1.setText(currentTotalSmsjava.getcountrynumber());



        ImageView iconView = (ImageView) listItemView.findViewById(R.id.img_item);
        iconView.setImageResource(currentTotalSmsjava.getcountryimage());
        return listItemView;


    }
}


