package com.androidexample.totalsmsdemo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseFragment {

    private static int SPLASH_TIME_OUT = 2000;

    public SplashFragment() {
        // Required empty public constructor
    }


    @Override
    public void setToolbarForFragment() {
        ((MainActivity)getActivity()).getToolbar().setVisibility(View.GONE);
    }

    @Override
    public void setTabForFragment() {
        ((MainActivity)getActivity()).getTabLayout().setVisibility(View.GONE);

    }
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_splash, container, false);
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
              mainInterface.Openhome();
            }
        }, SPLASH_TIME_OUT);
        return v;
    }

}
