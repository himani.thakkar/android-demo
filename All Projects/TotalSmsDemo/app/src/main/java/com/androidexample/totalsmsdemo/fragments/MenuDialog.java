package com.androidexample.totalsmsdemo.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.MainInterface;
import com.androidexample.totalsmsdemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDialog extends DialogFragment {

   View v;
    Activity activity;
    TextView tv_more;
    TextView tv_more2;
    MainInterface mainInterface;

    public MenuDialog() {
        super();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.dialog_more, container, false);
        mainInterface = (MainInterface)(MainActivity)getActivity();

        // Do all the stuff to initialize your custom view
//        FragmentTransaction ft = getFragmentManager().beginTransaction();
//        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
//        if (prev != null) {
//            ft.remove(prev);
//        }
//        ft.addToBackStack(null);
//
//        DialogFragment dialogFragment = new MenuDialog();
//        dialogFragment.show(ft, "dialog");
        tv_more = v.findViewById(R.id.tv_more);
        tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.Opencalltestsummary();
            }
        });

        tv_more2 = v.findViewById(R.id.tv_more2);
        tv_more2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainInterface.OpenSmstestsummary();
            }
        });


        return v;
    }
}
