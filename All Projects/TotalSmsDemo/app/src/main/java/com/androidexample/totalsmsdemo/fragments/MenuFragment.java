package com.androidexample.totalsmsdemo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends BaseFragment {

    View v;
    public MenuFragment() {

    }


    @Override
    public void setToolbarForFragment() {

    }

    @Override
    public void setTabForFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_menu, container, false);
        ((MainActivity) getActivity()).getToolbar().setVisibility(View.VISIBLE);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Menu Test");
 return v;
    }

}
