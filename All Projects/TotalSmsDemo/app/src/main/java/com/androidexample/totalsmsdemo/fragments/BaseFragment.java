package com.androidexample.totalsmsdemo.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidexample.totalsmsdemo.MainActivity;
import com.androidexample.totalsmsdemo.MainInterface;
import com.androidexample.totalsmsdemo.R;


public abstract class BaseFragment extends Fragment {

    public abstract void setToolbarForFragment();
    public abstract void setTabForFragment();
    MainInterface mainInterface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainInterface = (MainInterface) getActivity();
        setTabForFragment();
        setToolbarForFragment();
    }
}