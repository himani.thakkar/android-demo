package com.androidexample.sortingapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    String[] months = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };

    ArrayAdapter<String> adapter;
    ArrayList<String> list;
    JavaAdapter javaAdapter;

    ArrayList<Modelclass> callModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Spinner spinner = (Spinner) findViewById(R.id.planets_spinner);

        List<String> categories = new ArrayList<String>();
        categories.add("Select");
        categories.add("Ascending By Tier");
        categories.add("Decending By Tier");
        categories.add("Ascending By Country");
        categories.add("Decending By Country");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


        callModelList = new ArrayList<Modelclass>();
        callModelList.add(new Modelclass("+71 7043169995", "India"));
        callModelList.add(new Modelclass("+91 8154936235", "India"));
        callModelList.add(new Modelclass("+51 8160058122", "USA"));
        callModelList.add(new Modelclass("+91 8154936235", "Spain"));
        callModelList.add(new Modelclass("+31 8160058122", "UK"));
        callModelList.add(new Modelclass("+91 8154936235", "Rasia"));

    }


//    @Override
//    public void onItemSected(AdapterView<?> parent, View view, int position, long id) {
//        // On selecting a spinner item
//        String item = parent.getItemAtPosition(position).toString();
//
//        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();



    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub


        switch (item.getItemId()) {
            case R.id.sortasc:
//                list = new ArrayList<String>(Arrays.asList(callModelList.));
                Log.e("MainActivity","UnSorted List: "+list);
//                Collections.sort(callModelList, new Comparator<Modelclass>() {
//                    public int compare(Modelclass lhs, Modelclass rhs) {
//                        return (lhs.getCountry().compareTo(rhs.getCountry()));
//                    }
//                });

                Collections.sort(
                        callModelList,
                        new Comparator<Modelclass>()
                        {
                            public int compare(Modelclass lhs, Modelclass rhs)
                            {
                                return lhs.getCountry().compareTo(rhs.getCountry());
                            }
                        }
                );

                for (int i = 0; i < list.size(); i++) {
                    Log.e("MainActivity","Sorted List: "+list.get(i));
                }
//                adapter.clear();
                adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, list);
                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
                return true;
            case R.id.sortdesc:
                list = new ArrayList<String>(Arrays.asList(months));
                Log.e("MainActivity","Sorted List: "+list);
                Collections.sort(list, Collections.reverseOrder());
                for (int i = 0; i < list.size(); i++) {
                    Log.e("MainActivity","UnSorted List: "+list.get(i));
                }
//                adapter.clear();
                adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, list);
                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
