package com.androidexample.fruitapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv_gujarati_thali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        clickListeners();
    }

    private void clickListeners() {

    }

    private void initViews() {
        tv_gujarati_thali = findViewById(R.id.tv_gujarati_thali);
    }

    public void tv_gujarati_thali(View view){
    Intent i=new Intent(this,fruitName.class);
    startActivity(i);
}

}
