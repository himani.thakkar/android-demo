package com.androidexample.fruitapp;

public class Fruit {

    private  String frt_id;
    private  String frt_name;
    private  String frt_color;
    private  String frt_test;

    public Fruit(String frt_id, String frt_name, String frt_color, String frt_test) {
        this.frt_id = frt_id;
        this.frt_name = frt_name;
        this.frt_color = frt_color;
        this.frt_test = frt_test;
    }


    public String getFrt_id() {
        return frt_id;
    }

    public void setFrt_id(String frt_id) {
        this.frt_id = frt_id;
    }

    public String getFrt_name() {
        return frt_name;
    }

    public void setFrt_name(String frt_name) {
        this.frt_name = frt_name;
    }

    public String getFrt_color() {
        return frt_color;
    }

    public void setFrt_color(String frt_color) {
        this.frt_color = frt_color;
    }

    public String getFrt_test() {
        return frt_test;
    }

    public void setFrt_test(String frt_test) {
        this.frt_test = frt_test;
    }




}




